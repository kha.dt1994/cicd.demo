#!/bin/sh
set -xe

#Login to registry
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
#sudo docker login -u $CI_JOB_USER -p $CI_JOB_TOKEN $CI_REGISTRY

#docker build image
docker build -t ${CONTAINER_RELEASE_IMAGE} -f Dockerfile .

#docker push image to registry
docker push ${CONTAINER_RELEASE_IMAGE}

#docker remove image to registry
docker rmi ${CONTAINER_RELEASE_IMAGE}
#docker image prune -a -f
