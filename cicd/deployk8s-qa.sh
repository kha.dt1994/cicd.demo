#!/bin/bash
set -xe
# Deploy to k8s cluster
folder=$(pwd)

#Generate k8s folder from template
cd k8s;
#Default variable is on fpt-qa branch
IMAGE=$(echo ${CONTAINER_RELEASE_IMAGE} | sed -r 's/\//\\\//g');
sed -e "s/%NAMESPACE%/${NAMESPACE}/g" -e "s/%STS_NAME%/${STS_NAME}/g" -e "s/%EFS%/${EFS}/g" -e "s/%CONTAINERPORT%/${CONTAINERPORT}/g" -e "s/%MEMREQUEST%/${MEMREQUEST}/g" -e "s/%MEMLIMIT%/${MEMLIMIT}/g" -e "s/%CPUREQUEST%/${CPUREQUEST}/g" -e "s/%CPULIMIT%/${CPULIMIT}/g" -e "s/%CONTAINER_RELEASE_IMAGE%/${IMAGE}/g" stateful-template.yaml > stateful.yaml;
sed -e "s/%NAMESPACE%/${NAMESPACE}/g" -e "s/%CONTAINERPORT%/${CONTAINERPORT}/g" -e "s/%STS_NAME%/${STS_NAME}/g" svc-template.yaml > svc.yaml;
sed -e "s/%NAMESPACE%/${NAMESPACE}/g" -e "s/%CONTAINERPORT%/${CONTAINERPORT}/g" -e "s/%STS_NAME%/${STS_NAME}/g" ingress-template.yaml > ingress.yaml;
cat svc.yaml;
cat stateful.yaml;
#cat ingress.yaml

#Deploy
namespace=$(kubectl get ns | grep ${NAMESPACE} | awk '{print $1}' | head -1);
if [ ${namespace} = ${NAMESPACE} ]
then
  stsName=$(kubectl get sts -n ${NAMESPACE} | grep ${STS_NAME} | awk '{print $1}'| head -1);
  if [[ ${stsName} = ${STS_NAME} ]]
  then
    #Check if sts running or not
    stsStatus=$(kubectl get statefulset -n ${NAMESPACE} ${STS_NAME} | awk '{print $2}');
    #Update sts
    kubectl patch statefulset -n ${NAMESPACE} ${STS_NAME} -p '{"spec":{"updateStrategy":{"type":"RollingUpdate"}}}';
    kubectl patch statefulset -n ${NAMESPACE} ${STS_NAME} --type="json" -p="[{'op': 'replace', 'path': '/spec/template/spec/containers/0/image', 'value':'${CONTAINER_RELEASE_IMAGE}'}]";
    #If not running, need to delete pod to apply latests version
    if [[ $stsStatus == *"0/"* ]]
    then
      echo 'StatefulSet is not running, delete pod to apply latest image';
      kubectl get pod -n ${NAMESPACE} | awk '{print $1}' | grep ${STS_NAME} | xargs kubectl delete pod -n ${NAMESPACE};
    else
      echo "Running";
    fi
  else
    echo ""
    # kubectl apply -f stateful.yaml;
  fi
else
  #Create ns, sts, svc
  kubectl create ns ${NAMESPACE};
fi
kubectl apply -f stateful.yaml;
kubectl apply -f svc.yaml;
# kubectl apply -f ingress.yaml;
